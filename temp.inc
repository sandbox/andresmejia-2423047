<?php

///////////////// Formatters ///////////////////////

/**
 * Implements hook_field_formatter_info().
 */
function municipio_field_field_formatter_info() {
  return array(
    'municipio_field_default' => array(
      'label' => t('Default'),
      'field types' => array('municipios'),
    ),
    'municipio_field_table' => array(
      'label' => t('Show as table'),
      'field types' => array('municipios'),
      'settings' => array('units_as' => 'column'),
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function municipio_field_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $summary = '';

  if ($display['type'] == 'municipio_field_table') {
    if ($settings['units_as'] == 'column') {
      $summary = t('Show units as their own column');
    }
    else if ($settings['units_as'] == 'cell') {
      $summary = t('Show units in each cell');
    }
    else if ($settings['units_as'] == 'none') {
      $summary = t('Do not show units');
    }
  }
    
  return $summary;
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function municipio_field_field_formatter_settings_form($field, $instance, $view_mode, $form, &$form_state) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];

  $form = array();

  if ($display['type'] == 'municipio_field_table') {
    $form['units_as'] = array(
      '#title' => t('Show Municipio'),
      '#type' => 'select',
      '#options' => array(
        'column' => t('As their own column'),
        'cell' => t('In each cell'),
        'none' => t('Do not show units'),
      ),
      '#default_value' => $settings['units_as'],
      '#required' => TRUE,
    );
  }

  return $form;
}

/**
 * Implements hook_field_formatter_view().
 */
function municipio_field_field_formatter_view($obj_type, $object, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  switch ($display['type']) {
    case 'municipio_field_default':
		foreach ($items as $delta => $item) {
          $output = t(' @municipio  isodepto @isodepto departamento @departamento', array(
            '@municipio' => $item['municipio'],
            '@isodepto' => $item['isodepto'],
            '@departamento' => $item['departamento'],
          ));			
		}
		$element[$delta] = array('#markup' => $output);
      break;
    case 'municipio_field_table':
      $rows = array();
      foreach ($items as $delta => $item) {
        $row = array();
        if ($settings['units_as'] == 'cell') {
          $row[] = t('(%units) @value', array(
            '@value' => $item['municipio'],
          ));
          $row[] = t('@value', array(
            '@value' => $item['departamento'],
          ));
        }
        else {         
		  $row[] = $item['municipio'];
		  $row[] = $item['departamento'];
          $row[] = $item['isodepto'];
        }

        $rows[] = $row;
      }

      
      if ($settings['units_as'] == 'column') {
		  $header = array(t('Municipio'), t('Departamento'), t('ISO'));
      }	  else {
		  $header = array(t('Municipio'), t('Departamento'), t('ISO'));
	  }
	  
      $element = array(
        '#theme' => 'table',
        '#rows' => $rows,
        '#header' => $header,
      );
      break;
  }

  return $element;
}